<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CarteRepository")
 * @ApiResource
 */
class Carte
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $codeCentre;

    /**
     * @ORM\Column(type="string", length=6)
     */
    private $codeCarte;

    /**
     * @ORM\Column(type="smallint")
     */
    private $checksum;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $NFC;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="cartes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $client;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodeCentre(): ?string
    {
        return $this->codeCentre;
    }

    public function setCodeCentre(string $codeCentre): self
    {
        $this->codeCentre = $codeCentre;

        return $this;
    }

    public function getCodeCarte(): ?string
    {
        return $this->codeCarte;
    }

    public function setCodeCarte(string $codeCarte): self
    {
        $this->codeCarte = $codeCarte;

        return $this;
    }

    public function getChecksum(): ?int
    {
        return $this->checksum;
    }

    public function setChecksum(int $checksum): self
    {
        $this->checksum = $checksum;

        return $this;
    }

    public function getNFC(): ?string
    {
        return $this->NFC;
    }

    public function setNFC(string $NFC): self
    {
        $this->NFC = $NFC;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param mixed $client
     */
    public function setClient($client): void
    {
        $this->client = $client;
    }

    public static function calculeChecksum(string $codeCentre, string $codeCarte) : int
    {
        $res = 0;

        $codeCentreArray = str_split($codeCentre);
        foreach ($codeCentreArray as $chiffre) {
            $res += intval($chiffre);
        }

        $codeCarteArray = str_split($codeCarte);
        foreach ($codeCarteArray as $chiffre) {
            $res += intval($chiffre);
        }

        $res = $res % 9;

        return $res;
    }
}
