<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CentreRepository")
 * @ApiResource
 */
class Centre
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=3, unique=true)
     */
    private $codeCentre;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=80)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $telephone;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="centres")
     */
    private $clients;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Partie", mappedBy="centre")
     */
    private $parties;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getClients()
    {
        return $this->clients;
    }

    /**
     * @param mixed $clients
     */
    public function setClients($clients): void
    {
        $this->clients = $clients;
    }

    /**
     * @return mixed
     */
    public function getParties()
    {
        return $this->parties;
    }

    /**
     * @param mixed $parties
     */
    public function setParties($parties): void
    {
        $this->parties = $parties;
    }

    /**
     * @return mixed
     */
    public function getCodeCentre()
    {
        return $this->codeCentre;
    }

    /**
     * @param mixed $codeCentre
     */
    public function setCodeCentre($codeCentre): void
    {
        $this->codeCentre = $codeCentre;
    }
}
