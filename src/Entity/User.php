<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ApiResource
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $surnom;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=80)
     */
    private $email;

    /**
     * @ORM\Column(type="array")
     */
    private $roles = [];

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateNaissance;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateEnregistrement;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateDernierePartie;

    /**
     * @ORM\Column(type="boolean")
     */
    private $actif;

    /**
     * @ORM\Column(type="integer")
     */
    private $niveau;

    /**
     * @ORM\Column(type="integer")
     */
    private $experience;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbVictimes;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbMorts;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $justesse;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Carte", mappedBy="client")
     * @var Collection
     */
    private $cartes;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Equipe", inversedBy="joueurs")
     * @ORM\JoinColumn(nullable=true)
     */
    private $equipe;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Centre", inversedBy="clients")
     * @ORM\JoinTable(name="users_centres")
     */
    private $centres;

    public function __construct()
    {
        $this->cartes       = new ArrayCollection();
        $this->actif        = true;
        $this->niveau       = 1;
        $this->experience   = 0;
        $this->nbVictimes   = 0;
        $this->nbMorts      = 0;
        $this->justesse     = 0;
    }

    /**
     * @param int|null $id
     * @param string $surnom
     * @param string $email
     * @param string $password
     * @param array $roles
     * @param \DateTime|null $dateEnregistrement
     * @return User
     */
    static public function creer(
        ?int    $id = null,
        string  $surnom,
        string  $email,
        string  $password,
        array   $roles,
        ?\DateTime $dateEnregistrement = null
    )
    {
        $user = new self();

        $user->id       = $id;
        $user->surnom   = $surnom;
        $user->email    = $email;
        $user->password = $password;
        $user->roles    = $roles;
        $user->dateEnregistrement = $dateEnregistrement;

        return $user;
    }

    public function addCarte(Carte $carte)
    {
        if ($this->cartes->contains($carte)) {
            return;
        }
        $this->cartes->add($carte);
        $carte->setClient($this);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSurnom(): ?string
    {
        return $this->surnom;
    }

    public function setSurnom(string $surnom): self
    {
        $this->surnom = $surnom;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getRoles(): ?array
    {
        return $this->roles;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getDateNaissance(): ?\DateTimeInterface
    {
        return $this->dateNaissance;
    }

    public function setDateNaissance(\DateTimeInterface $dateNaissance): self
    {
        $this->dateNaissance = $dateNaissance;

        return $this;
    }

    public function getDateEnregistrement(): ?\DateTimeInterface
    {
        return $this->dateEnregistrement;
    }

    public function setDateEnregistrement(\DateTimeInterface $dateEnregistrement): self
    {
        $this->dateEnregistrement = $dateEnregistrement;

        return $this;
    }

    public function getDateDernierePartie(): ?\DateTimeInterface
    {
        return $this->dateDernierePartie;
    }

    public function setDateDernierePartie(?\DateTimeInterface $dateDernierePartie): self
    {
        $this->dateDernierePartie = $dateDernierePartie;

        return $this;
    }

    public function getActif(): ?bool
    {
        return $this->actif;
    }

    public function setActif(bool $actif): self
    {
        $this->actif = $actif;

        return $this;
    }

    public function getNiveau(): ?int
    {
        return $this->niveau;
    }

    public function setNiveau(int $niveau): self
    {
        $this->niveau = $niveau;

        return $this;
    }

    public function getExperience(): ?int
    {
        return $this->experience;
    }

    public function setExperience(int $experience): self
    {
        $this->experience = $experience;

        return $this;
    }

    public function getNbVictimes(): ?int
    {
        return $this->nbVictimes;
    }

    public function setNbVictimes(int $nbVictimes): self
    {
        $this->nbVictimes = $nbVictimes;

        return $this;
    }

    public function getNbMorts(): ?int
    {
        return $this->nbMorts;
    }

    public function setNbMorts(int $nbMorts): self
    {
        $this->nbMorts = $nbMorts;

        return $this;
    }

    public function getJustesse(): ?int
    {
        return $this->justesse;
    }

    public function setJustesse(int $justesse): self
    {
        $this->justesse = $justesse;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCartes()
    {
        return $this->cartes;
    }

    /**
     * @param mixed $cartes
     */
    public function setCartes($cartes): void
    {
        $this->cartes = $cartes;
    }

    /**
     * @return mixed
     */
    public function getEquipe()
    {
        return $this->equipe;
    }

    /**
     * @param mixed $equipe
     */
    public function setEquipe($equipe): void
    {
        $this->equipe = $equipe;
    }

    /**
     * @return mixed
     */
    public function getCentres()
    {
        return $this->centres;
    }

    /**
     * @param mixed $centre
     */
    public function setCentres($centres): void
    {
        $this->centres = $centres;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        return $this->surnom;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
    }
}
