<?php
/**
 * Created by PhpStorm.
 * User: Etudiant
 * Date: 06/12/2018
 * Time: 12:39
 */

namespace App\DataFixtures;


use App\Entity\Partie;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class PartieFixtures extends Fixture implements DependentFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * Creation de 25 parties (5 parties par centre (5 centres), 3 equipes (75 equipes) par partie)
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        for($i = 0; $i < 25; $i++) {
            $partie = new Partie();
            $partie->setActive(rand(0,1));
            $centre = $this->getReference('centre'.rand(0,4));
            $partie->setCentre($centre);

            $this->addReference('partie'.$i, $partie);

            $manager->persist($partie);
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            CentreFixtures::class,
        ];
    }
}
