<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        for($i = 0; $i < 50; $i++) {
            $user = new User();
            $user->setSurnom('user_'.$i);
            $user->setNom('nom_'.$i);
            $user->setPrenom('prenom_'.$i);
            $user->setPassword(password_hash('user', PASSWORD_BCRYPT));
            $user->setEmail('user'.$i.'@fake.fr');
            $user->setDateEnregistrement(new \DateTime('-'.$i.' days'));
            $user->setDateDernierePartie(new \DateTime('-'.$i.' days'));
            $user->setDateNaissance(new \DateTime('-'.($i*1000).' days'));
            $user->setActif(rand(0,1));
            $user->setExperience(rand(0,100));
            $user->setNiveau(rand(0,100));
            $user->setJustesse(rand(0, 100));
            $user->setNbMorts(rand(0, 100));
            $user->setNbVictimes(rand(0, 100));
            //$centre = $this->getReference('centre'.rand(0,4));
            //$user->setCentres([$centre]); // fait dans CarteFixtures
            $user->setRoles(['ROLE_USER']);
            $equipe = $this->getReference('equipe'.rand(0,74));
            $user->setEquipe($equipe);
            // addReference garde en référence notre $user sous un certain nom
            // de façon à le rendre disponible dans les autres fixtures
            $this->addReference('user'.$i, $user);
            // $manager persist demande à doctrine de préparer l'insertion de
            // l'entité en base de données -> INSERT INTO
            $manager->persist($user);
        }

        $admin = new User();
        $admin->setSurnom('root');
        $admin->setNom('nom_admin');
        $admin->setPrenom('prenom_admin');
        $admin->setPassword(password_hash('admin', PASSWORD_BCRYPT));
        $admin->setEmail('admin@shinigamilaser.fr');
        $admin->setDateEnregistrement(new \DateTime('now'));
        $admin->setDateDernierePartie(new \DateTime('now'));
        $admin->setDateNaissance(new \DateTime('-'.(20000).' days'));
        $admin->setActif(false);
        $admin->setExperience(0);
        $admin->setNiveau(0);
        $admin->setRoles(['ROLE_ADMIN']);
        $this->addReference('root', $admin);
        $manager->persist($admin);

        $superAdmin = new User();
        $superAdmin->setSurnom('superroot');
        $superAdmin->setNom('nom_super_admin');
        $superAdmin->setPrenom('prenom_super_admin');
        $superAdmin->setPassword(password_hash('superadmin', PASSWORD_BCRYPT));
        $superAdmin->setEmail('superadmin@shinigamilaser.fr');
        $superAdmin->setDateEnregistrement(new \DateTime('now'));
        $superAdmin->setDateDernierePartie(new \DateTime('now'));
        $superAdmin->setDateNaissance(new \DateTime('-'.(20000).' days'));
        $superAdmin->setActif(false);
        $superAdmin->setExperience(0);
        $superAdmin->setNiveau(0);
        $superAdmin->setRoles(['ROLE_SUPER_ADMIN']);
        $this->addReference('superroot', $superAdmin);
        $manager->persist($superAdmin);
        // flush() valide les requêtes SQL et les execute
        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            EquipeFixtures::class,
            CentreFixtures::class,
        ];
    }
}
