<?php
/**
 * Created by PhpStorm.
 * User: Etudiant
 * Date: 06/12/2018
 * Time: 12:30
 */

namespace App\DataFixtures;


use App\Entity\Equipe;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class EquipeFixtures extends Fixture
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * creation de 75 equipes
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $equipes = [];

        for($i = 0; $i < 75; $i++) {
            $equipe = new Equipe();
            $equipe->setPoints(rand(0,100));

            array_push($equipes, $equipe);

            $this->addReference('equipe'.$i, $equipe);

            $manager->persist($equipe);
        }

//        for($i = 0; $i < 50; $i++) {
//            /** @var User $joueur */
//            $joueur = $this->getReference('user'.$i);
//            if ($joueur->getActif()) {
//                $equipes[rand(0,74)]->addJoueur($joueur);
//            }
//        }

        $manager->flush();
    }
}
