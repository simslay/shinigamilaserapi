<?php
/**
 * Created by PhpStorm.
 * User: Etudiant
 * Date: 06/12/2018
 * Time: 14:36
 */

namespace App\DataFixtures;


use App\Entity\Centre;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class CentreFixtures extends Fixture #implements DependentFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * Creation de 5 centres
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        for($i = 0; $i < 5; $i++) {
            $centre = new Centre();

            $centre->setNom("centre".$i);
            $centre->setEmail("centre".$i."@shinigamilaser.fr");
            $centre->setAdresse("");
            $centre->setTelephone("");
            $centre->setCodeCentre("00".$i);

//            $k = $i * 10;
//            $clients = [];
//            for($j = $k; $j < $k+10; $j++) {
//                array_push($clients, $this->getReference('user'.$j));
//            }
//            $centre->setClients($clients);

//            $k = $i * 5;
//            $parties = [];
//            for($j = $k; $j < $k+5; $j++) {
//                array_push($parties, $this->getReference('partie'.$j));
//            }
//            $centre->setParties($parties);

            $this->addReference('centre'.$i, $centre);

            $manager->persist($centre);
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            #UserFixtures::class,
            #PartieFixtures::class,
        ];
    }
}
