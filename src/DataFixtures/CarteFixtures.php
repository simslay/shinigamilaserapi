<?php
/**
 * Created by PhpStorm.
 * User: simslay
 * Date: 06/12/2018
 * Time: 07:27
 */

namespace App\DataFixtures;


use App\Entity\Carte;
use App\Entity\Centre;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class CarteFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * Creation de 50 cartes pour 50 clients
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $centreRepository = $manager->getRepository(Centre::class);

        for($i = 0; $i < 50; $i++) {
            $centres = [];
            $codesCentres = [];
            $carte1 = $this->creerCarte($codesCentres);
            $manager->persist($carte1);

            /** @var User $client */
            $client = $this->getReference('user'.$i);

            $client->addCarte($carte1);

            $centre1 = $centreRepository->findOneBy([
                'codeCentre' => $carte1->getCodeCentre()
            ]);
            array_push($centres, $centre1);
            array_push($codesCentres, $carte1->getCodeCentre());

            if (rand(0,10) > 5) {
                $carte2 = $this->creerCarte($codesCentres);
                $manager->persist($carte2);
                $client->addCarte($carte2);

                $centre2 = $centreRepository->findOneBy([
                    'codeCentre' => $carte2->getCodeCentre()
                ]);
                array_push($centres, $centre2);
                array_push($codesCentres, $carte2->getCodeCentre());
            }

            if (rand(0,10) > 5) {
                $carte3 = $this->creerCarte($codesCentres);
                $manager->persist($carte3);
                $client->addCarte($carte3);

                $centre3 = $centreRepository->findOneBy([
                    'codeCentre' => $carte3->getCodeCentre()
                ]);
                array_push($centres, $centre3);
                //array_push($codesCentres, $carte3->getCodeCentre());
            }

            $client->setCentres($centres);
        }

        $manager->flush();
    }

    private function creerCarte(array $codesCentres) : Carte
    {
        $carte = new Carte();
        $carte->setActive(rand(0,1));

        $codeCentre = "00".rand(0,4);

        while(true) {
            if (false === in_array($codeCentre, $codesCentres)) {
                break;
            }

            $codeCentre = "00".rand(0,4);
        }

        $codeCarte = "";
        for($j = 0; $j < 6; $j++) {
            $codeCarte .= rand(0, 9);
        }

        $carte->setCodeCentre($codeCentre);
        $carte->setCodeCarte($codeCarte);
        $carte->setChecksum(
            Carte::calculeChecksum(
                $carte->getCodeCentre(),
                $carte->getCodeCarte()
            )
        );

        $NFC = "";
        for($j = 0; $j < 64; $j++) {
            $NFC .= rand(0, 9);
        }

        $carte->setNFC($NFC);

        return $carte;
    }

    public function getDependencies(): array
    {
        return [
            UserFixtures::class,
        ];
    }
}
